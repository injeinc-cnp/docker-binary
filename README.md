```
$ ls
Dockerfile  eap73-openjdk11-basic-docker-binary.yaml  sht-1.0-SNAPSHOT.war
```

```
$ oc new-app -f eap73-openjdk11-basic-docker-binary.yaml
--> Deploying template "moricom-dev/eap73-openjdk11-basic-docker-binary" for "eap73-openjdk11-basic-docker-binary.yaml" to project moricom-dev

     JBoss EAP 7.3 (OpenJDK 11) for Docker Build (Source Input Binary)
     ---------
     An example JBoss Enterprise Application Platform application. For more information about using this template, see https://github.com/jboss-container-images/jboss-eap-7-openshift-image/blob/7.3.x/README.adoc

     A new JBoss EAP based application has been created in your project.

     * With parameters:
        * Application Name=eap-app
        * EAP Image Name=jboss-eap73-openjdk11-openshift:7.3
        * Enable ExampleDS datasource=false
        * Queues=
        * Topics=
        * AMQ cluster password=gXJ8Sm1E # generated
        * GitLab Webhook Secret=5Iom1BOG # generated
        * Github Webhook Secret=rPqv1Mle # generated
        * Generic Webhook Secret=lpMvD072 # generated
        * ImageStream Namespace=openshift
        * JGroups Cluster Password=U0xXXqKv # generated
        * Deploy Exploded Archives=false
        * TZ=Asia/Seoul
        * JAVA_OPTS_APPEND=-Dfile.encoding=UTF-8
        * MEMORY_LIMIT=1Gi

--> Creating resources ...
    service "eap-app" created
    service "eap-app-ping" created
    route.route.openshift.io "eap-app" created
    imagestream.image.openshift.io "eap-app" created
    buildconfig.build.openshift.io "eap-app" created
    deploymentconfig.apps.openshift.io "eap-app" created
--> Success
    Access your application via route 'eap-app-moricom-dev.apps.sandbox.x8i5.p1.openshiftapps.com'
    Build scheduled, use 'oc logs -f bc/eap-app' to track its progress.
    Run 'oc status' to view your app.
```

```
$ oc start-build eap-app --from-dir=.
Uploading directory "." as binary input for the build ...
......
Uploading finished
build.build.openshift.io/eap-app-2 started
```
