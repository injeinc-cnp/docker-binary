FROM
COPY *.war /deployments/ROOT.war
USER root
RUN chown -R 185:root /deployments/ROOT.war && chmod 664 /deployments/ROOT.war
USER jboss
CMD $JBOSS_HOME/bin/openshift-launch.sh